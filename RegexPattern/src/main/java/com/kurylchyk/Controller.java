package com.kurylchyk;

import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.Map;
import org.apache.logging.log4j.*;
public class Controller {

    private static Scanner sc = new Scanner(System.in);
    private String str;
    private Map<String,String> menu;
    private Map<String, View>  methodMenu;
    private View view;
    private static Logger logger = LogManager.getLogger(Controller.class);
    Controller(){
        view = (s)-> logger.info(s);
        menu = new LinkedHashMap<String, String>();
        methodMenu = new LinkedHashMap<String, View>();
        System.out.println("Enter the string:");
        str = sc.nextLine();
        menu.put("1","1 - Check if the str begins with a capital letter and ends with a period.");
        menu.put("2","2 - Split the string on the words \"the\" or \"you\".");
        menu.put("3","3 - Replace all the vowels ");
        menu.put("Q","Q - exit");
        showMenu();
        putAnswer();
    }

    public void showMenu(){

        for(String element: menu.values()){
            System.out.println(element);
        }
    }

    public void putAnswer(){



        String keyMenu ="";
        do {

            System.out.println("Select appropriate option");
            keyMenu = sc.nextLine().toUpperCase();

            if(keyMenu.equals("Q")) {
                System.exit(0);
            } else {

                switch (keyMenu){

                    case "1": view.show(CheckPattern.hasCapitalAndPeriod(str)?"It does":"It does not"); break;
                    case "2":

                        String[] words = CheckPattern.split(str);
                        String allTheWords="";
                        for(String element:words){
                            allTheWords+=element;
                        }
                        view.show(allTheWords); break;
                    case "3": view.show(CheckPattern.replaceWithUnderScore(str)); break;
                }
            }
        } while (keyMenu != "Q");


    }


}
