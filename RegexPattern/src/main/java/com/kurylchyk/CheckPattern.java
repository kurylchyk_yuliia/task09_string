package com.kurylchyk;

public class CheckPattern {

   public static boolean  hasCapitalAndPeriod(String str) {

      return  str.matches("^[A-Z][^\\.]*\\.$");
    }

    public static String[] split(String str){

       String splitOn = "the|you";

       String[] words = str.split(splitOn);

       return words;
    }

    public static String replaceWithUnderScore(String str) {


        String pattern = "[aeiuoy]";
       return  str.replaceAll(pattern, "_vowel_");

    }

}
