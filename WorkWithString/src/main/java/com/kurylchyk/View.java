package com.kurylchyk;

@FunctionalInterface
public interface View {

    void showMenu();

}
