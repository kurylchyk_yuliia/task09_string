package com.kurylchyk;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Menu implements  View {
    private Map<String, String> menu;
    private Map<String, View> methodMenu;
    private static Scanner sc = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(Menu.class);

    private ResourceBundle bundle;
    private Locale locale;
    private Controller controller = new Controller();


    private void setMenu() {

        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("Q", bundle.getString("Q"));
    }

    public Menu() {

        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::showMenu);
        methodMenu.put("2", this::showMenu);
        methodMenu.put("3", this::setUkrainianMenu);
        methodMenu.put("4", this::setCzechMenu);
        methodMenu.put("5", this::setTurkishMenu);
        methodMenu.put("6", this::setGreekMenu);
        methodMenu.put("7", this::setEnglishMenu);

        showMenu();
        putAnswer();

    }



    public void setUkrainianMenu() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        showMenu();
    }

    public void setCzechMenu() {
        locale = new Locale("cs");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        showMenu();
    }

    public void setTurkishMenu() {
        locale = new Locale("tr");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();

        showMenu();
    }

    public void setGreekMenu() {
        locale = new Locale("el");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        showMenu();
    }

    public void setEnglishMenu() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        showMenu();
    }

    @Override
    public void showMenu() {
        for (String element : menu.values()) {
            System.out.println(element);
        }
    }

    public void putAnswer(){

        String keyMenu ="";
        do {

            System.out.println("Select appropriate option");
            keyMenu = sc.nextLine().toUpperCase();

            if(keyMenu.equals("1")||keyMenu.equals("2")) {
               actionWithController(keyMenu);
            }

           else  if(keyMenu.equals("Q")) {

                System.exit(0);
            } else {
                try {
                    methodMenu.get(keyMenu).showMenu();
                } catch (Exception e) {
                }
            }
        } while (keyMenu != "Q");
    }


    public void actionWithController(String key){

        switch(key){
            case "1" : controller.putValue(); break;
            case "2":
                logger.info(controller.getValues());break;
        }

    }


}