package com.kurylchyk;


import java.util.Scanner;

public class Controller {


    private StringUtils stringUtils;



    public Controller() {

        stringUtils = new StringUtils();

    }


    void putValue() {
        Scanner sc = new Scanner(System.in);
        Object ob;
        do{
            System.out.println("Put the element: ");
            ob = sc.nextLine();
            stringUtils.putElement(ob);
        }while(!ob.toString().equals(""));

    }

    public String getValues() {
        return stringUtils.getStringUtils();
    }
}
