package com.kurylchyk;
import java.lang.String;

public class StringUtils {

  private   StringBuilder allTogether;

    StringUtils() {
        allTogether = new StringBuilder();
    }

    StringUtils(Object ... ob) {
        allTogether = new StringBuilder();
        putElement(ob);
    }

    void putElement(Object ... ob){
        for(Object element: ob){
            allTogether
                    .append(" ")
                    .append(element);
        }
    }

    String getStringUtils(){
        String str = allTogether.toString();
        return str;

    }
}
