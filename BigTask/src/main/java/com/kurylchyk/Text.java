package com.kurylchyk;

import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.Comparator.*;
import static java.util.Map.Entry.comparingByKey;
import static java.util.stream.Collectors.toMap;

public class Text {

    private String text;
    private Sentence sentence = new Sentence();
    private String[] textString;
    private List<String>[] allWords;

    public Text() {

        text = new String("");
    }

    public void getText() {

        try (FileReader reader = new FileReader("ThinkingInJava.txt")) {
            int c;
            while ((c = reader.read()) != -1) {

                text += (char) c;
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (java.io.IOException ex) {
            System.out.println(ex.getMessage());
        }

    }


    public void set() {

        textString = text.split("\\.|\\?");
        allWords = new ArrayList[textString.length];
        for (int index = 0; index < textString.length; index++) {
            allWords[index] = new ArrayList<>(Arrays.asList(textString[index].split(" ")));
        }

    }

    /**
     * Task 1
     *
     * @return
     */
    public void sentenceWithSameWords() {
        textString = text.split("\\.|\\?");
        for (String element : textString) {
            sentence.setSentence(element);
        }
        Integer theBiggestFrequency = 0;
        for (int index = 0; index < sentence.length(); index++) {
            System.out.println("\n" + (index + 1) + "\tSENTENCE ");
            List<String> list = Arrays.asList(sentence.getSentence(index).split(" "));
            Set<String> Words = new HashSet<String>(list);
            for (String word : Words) {
                if (Collections.frequency(list, word) > 1) {
                    if (theBiggestFrequency < Collections.frequency(list, word))
                        theBiggestFrequency = Collections.frequency(list, word) + 1;
                    System.out.print("[" + word + ": " + Collections.frequency(list, word) + "]\t");
                } else continue;
            }
        }

        System.out.println(theBiggestFrequency + "\tis a sentence with the biggest frequency of duplication");
    }


    /**
     * Task 2
     */

    public void countWordsUsingSplit() {
        Map<Integer, String> stringWithCountOfWords = new LinkedHashMap<>();

        for (int index = 0; index < sentence.length(); index++) {
            String[] words = sentence.getSentence(index).split(" ");
            stringWithCountOfWords.put(words.length, sentence.getSentence(index));
        }

        stringWithCountOfWords = stringWithCountOfWords.entrySet().stream()
                .sorted(comparingByKey())
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        for (String element : stringWithCountOfWords.values()) {
            System.out.println(element);
        }
    }

    /**
     * Task 3
     */

    public void findUniqueWordInFirstSentence() {

        List<String> wordsInFirstSentence = new ArrayList<String>(Arrays.asList((textString[0].split(" "))));

        for (int index = 1; index < sentence.length(); index++) {
            List<String> list = Arrays.asList(sentence.getSentence(index).split(" "));
            for (int currentInFirst = 0; currentInFirst < wordsInFirstSentence.size(); currentInFirst++) {
                for (int currentInOther = 0; currentInOther < list.size(); currentInOther++) {

                    if (currentInFirst == wordsInFirstSentence.size())
                        break;
                    if (wordsInFirstSentence.get(currentInFirst).compareTo(list.get(currentInOther)) == 0) {
                        wordsInFirstSentence.remove(currentInFirst);
                    }
                }
            }

        }


        System.out.print("All unique words: ");
        for (String element : wordsInFirstSentence) {
            System.out.print(element + "\t");

        }


    }


    /**
     * Task 4
     */

    public void sentenceWithQuestionMark(int length) {
        try {
            BufferedReader fr = new BufferedReader(new FileReader("ThinkingInJava.txt"));
            String line = null;
            List<String> words = new ArrayList<>();
            List<String> lines = new ArrayList<String>();
            while ((line = fr.readLine()) != null)
                lines.add(line);
            // System.out.println(lines);
            Pattern pattern = Pattern.compile("([^.!?]*)\\?");
            //^\w{length}$
            for (String tmp : lines) {
                Matcher m = pattern.matcher(tmp);
                while (m.find()) {
                    words.add(m.group());
                    System.out.println(m.group());
                }
            }

            Pattern patternForWord = Pattern.compile("^[a-z]{" + length + "}$");

            for (int index = 0; index < words.size(); index++) {
                List<String> eachWord = new ArrayList<>(Arrays.asList(words.get(index).split(" ")));
                for (String element : eachWord) {
                    Matcher matcher = patternForWord.matcher(element);
                    while (matcher.find()) {
                        System.out.println(matcher.group());
                    }

                }
            }
        } catch (IOException ex) {
            ex.getMessage();
        }
    }


    /**
     * Task 5
     */

    public void findTheLongestAndReplace() {


        Pattern pattern = Pattern.compile("^[AEIOU]");

        List<String> allSentence = new ArrayList<String>(Arrays.asList(textString));
        for (int index = 0; index < textString.length; index++) {
            List<String> allWordsInSentence = new ArrayList<>(Arrays.asList(allSentence.get(index).split(" ")));
            String theLongest = allWordsInSentence.stream().max(comparingInt(String::length)).get();

            if (allWordsInSentence.get(0).matches(("(?i)^[AEIOUY].*$"))) {
                allWordsInSentence.remove(0);
                allWordsInSentence.set(0, theLongest);
            }

            for (String element : allWordsInSentence) {
                System.out.print(element + "\t");
            }
        }
    }

    /**
     * Task 6
     */

    public void sortWordsInSentence() {


        for (int index = 0; index < textString.length; index++) {
            List<String> words = new ArrayList<>(Arrays.asList(textString[index].split(" ")));
            words = words.stream().sorted(String::compareToIgnoreCase).collect(Collectors.toList());

            System.out.print(index + 1 + " sentence:\t");

            for (String element : words) {
                System.out.print(element + "\t");
            }
            System.out.println();
        }

    }

    /**
     * Task 7
     */

    public void sortByPercentage() {

        Pattern pattern = Pattern.compile("[^aeiouAEIOU]");
        MultiMap<Double, String> sortedWordsByVowel = new MultiValueMap<>();
        for (int index = 0; index < textString.length; index++) {
            List<String> words = new ArrayList<>(Arrays.asList(textString[index].split(" ")));

            for (int jdex = 0; jdex < words.size(); jdex++) {
                Matcher matcher = pattern.matcher(words.get(jdex));
                if (matcher.find()) {
                    double countOfVowel = words.get(jdex).replaceAll("[^aeiouAEIOU]", "").length();
                    sortedWordsByVowel.put(((countOfVowel / words.get(jdex).length()) * 100), words.get(jdex));
                }
            }
        }

    }


    /**
     * Task 8
     */

    public void sortVowelWords() {

        List<String> vowelWords = new ArrayList<>();
        Pattern pattern = Pattern.compile("^[aeiouAEIOU]");

        for (int index = 0; index < textString.length; index++) {
            List<String> words = new ArrayList<>(Arrays.asList(textString[index].split(" ")));

            for (int jdex = 0; jdex < words.size(); jdex++) {

                Matcher matcher = pattern.matcher(words.get(jdex));
                if (matcher.find()) {

                    vowelWords.add(words.get(jdex));
                }
            }
            vowelWords = vowelWords.stream().sorted().collect(Collectors.toList());

            System.out.print(index + 1 + " sentence : ");
            for (String element : vowelWords) {
                System.out.print(element + "\t");
            }
            System.out.println();
            vowelWords.clear();
        }
    }

    /**
     * Task 9
     */

    public void sortByCountOfSymbol(char symbol) {
        Pattern pattern = Pattern.compile("[" + symbol + "]");
        Matcher matcher;
        MultiMap<Long, String> withSymbol = new MultiValueMap<>();

        for (int index = 0; index < textString.length; index++) {
            List<String> words = new ArrayList<>(Arrays.asList(textString[index].split(" ")));
            for (int jdex = 0; jdex < words.size(); jdex++) {

                matcher = pattern.matcher(words.get(jdex));
                if (matcher.find()) {
                    Long countOfSymbol = words.get(jdex).chars().filter(ch -> ch == symbol).count();
                    withSymbol.put(countOfSymbol, words.get(jdex));
                }

            }

        }

        sortMultiMap(withSymbol);

    }

    /**
     * For task 9
     *
     * @param multiMap
     */

    private void sortMultiMap(MultiMap multiMap) {

        multiMap.entrySet()
                .stream()
                .sorted(Map.Entry.<Long, String>comparingByKey())
                .forEach(System.out::println);
    }


    /**
     * Task 10
     */

    public void findTheWords() {

        List<String> words = new ArrayList<>();
        words.add("overloading");
        words.add("wash");
        words.add("method");
        words.add("arise");
        words.add("almost");


        Map<String, Integer> countOfWords = new LinkedHashMap<>();
        for (int index = 0; index < words.size(); index++) {
            countOfWords.put(words.get(index), 0);
        }


        for (int index = 0; index < allWords.length; index++) {
            for (int jdex = 0; jdex < allWords[index].size(); jdex++) {
                for (int k = 0; k < words.size(); k++) {
                    if (allWords[index].get(jdex).toLowerCase().compareTo(words.get(k)) == 0) {
                        System.out.println("Sentence " + (index + 1) + " has word :" + words.get(k));
                        countOfWords.replace(words.get(k), Integer.parseInt(String.valueOf(countOfWords.get(words.get(k)) + 1)));


                    }
                }
            }

        }


        countOfWords.entrySet()
                .stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue())
                .forEach(System.out::println);

    }


    /**
     * Task 11
     */

    public void removeSubString(char start, char end) {
        Pattern pattern = Pattern.compile("" + start + "(.*?)+" + end + "");
        String[] forDelete = textString.clone();

        String currentSentence = "";

        int length;
        int currentLength;
        int first = 0;
        int last = 0;
        boolean isHere;
        for (int index = 0; index < forDelete.length; index++) {
            currentSentence = forDelete[index];
            isHere = false;
            length = 0;
            currentLength = 0;
            System.out.println("\nSentence \t" + index);
            Matcher matcher = pattern.matcher(currentSentence);

            while (matcher.find()) {
                isHere = true;
                System.out.println(matcher.group());
                currentLength = matcher.end() - matcher.start();
                if (currentLength > length) {
                    first = matcher.start();
                    last = matcher.end();
                    length = currentLength;
                }
            }

            if (isHere) {

                String temp = forDelete[index];
                forDelete[index] = "";
                for (int i = 0; i < temp.length(); i++) {

                    if (i >= first && i <= last)
                        continue;
                    forDelete[index] += temp.charAt(i);
                }
            }
        }


        for (String element : forDelete) {
            System.out.println(element);
        }
    }

    /**
     * Task 12
     *
     * @param length
     */

    public void removeTheWord(int length) {

        List<String>[] words = allWords.clone();
        for (int index = 0; index < words.length; index++) {
            for (int jdex = 0; jdex < words[index].size(); jdex++) {

                Pattern patternChar = Pattern.compile("^[^aeiouAEIOU]");
                Matcher matcher = patternChar.matcher(words[index].get(jdex));
                if (matcher.find()) {
                    if (words[index].get(jdex).length() == length) {
                        words[index].remove(jdex);
                    }
                }
            }

        }

        for (int index = 0; index < words.length; index++) {
            for (int jdex = 0; jdex < words[index].size(); jdex++) {
                System.out.print(words[index].get(jdex) + "\t");
            }
            System.out.println("");
        }
    }

    /**
     * Task 13
     */

    public void sortWordsBySymbol(char symbol) {
        Pattern pattern = Pattern.compile("[" + symbol + "]");
        List<String>[] words = allWords.clone();
        List<String> oneSymbol = new ArrayList<>();
        List<String> twoSymbol = new ArrayList();
        List<String> threeSymbol = new ArrayList();

        for (int index = 0; index < words.length; index++) {
            for (int jdex = 0; jdex < words[index].size(); jdex++) {
                Matcher matcher = pattern.matcher(words[index].get(jdex).toLowerCase());
                if (matcher.find()) {
                    long count = words[index].get(jdex).toLowerCase().chars().filter(ch -> ch == symbol).count();
                    if (count == 1) {
                        oneSymbol.add(words[index].get(jdex));
                    }
                    if (count == 2) {
                        twoSymbol.add(words[index].get(jdex));
                    }
                    if (count == 3) {
                        threeSymbol.add(words[index].get(jdex));
                    }

                }
            }
        }


        System.out.print("1:[ ");
        oneSymbol = oneSymbol.stream().sorted().collect(Collectors.toList());
        for (String element : oneSymbol) {
            System.out.print(element + "\t");
        }
        System.out.println("]");

        System.out.print("2:[ ");
        twoSymbol = twoSymbol.stream().sorted().collect(Collectors.toList());
        for (String element : twoSymbol) {
            System.out.print(element + "\t");
        }
        System.out.println("]");

        System.out.print("3:[ ");
        threeSymbol = threeSymbol.stream().sorted().collect(Collectors.toList());
        for (String element : threeSymbol) {
            System.out.print(element + "\t");
        }
        System.out.println("]");


    }


    /**
     * Task 14
     */

    public void findPalindrome() {

        boolean isPalindrome;

        for (int index = 0; index < allWords.length; index++) {
            for (int jdex = 0; jdex < allWords[index].size(); jdex++) {

                isPalindrome = true;
                String element = allWords[index].get(jdex).toLowerCase();
                int i = 0, j = element.length() - 1;

                while (i < j) {
                    if (element.charAt(i) != element.charAt(j)) {
                        isPalindrome = false;
                        break;
                    }
                    i++;
                    j--;
                }
                if (isPalindrome && allWords[index].get(jdex).length() > 1) {
                    System.out.println("Sentence " + index + " has palindrome " + allWords[index].get(jdex));
                }
            }

        }


    }

    /**
     * Task 15
     */
    public void changeTheWords() {

        List<String>[] forChanging = allWords.clone();


        for (int index = 0; index < forChanging.length; index++) {
            for (int jdex = 0; jdex < forChanging[index].size(); jdex++) {


                if (forChanging[index].get(jdex).length() > 1) {
                    char first = forChanging[index].get(jdex).charAt(0);
                    char last = forChanging[index].get(jdex).charAt(forChanging[index].get(jdex).length() - 1);
                    String element = "";

                    for (int k = 0; k < forChanging[index].get(jdex).length(); k++) {

                            if ((forChanging[index].get(jdex).charAt(k) == first || forChanging[index].get(jdex).charAt(k) == last) && (k!=0 && k!=forChanging[index].get(jdex).length()-1)) {
                                continue;
                            } else {
                                element += forChanging[index].get(jdex).charAt(k);
                            }

                    }

                    forChanging[index].remove(jdex);
                    forChanging[index].add(jdex, element);

                }
            }

            System.out.print(index+" sentence \t: ");
            for (String el : forChanging[index]) {
                System.out.print(el + "\t");
            }
            System.out.println("");
        }
    }


    /**
     * Task 16
     *
     */

    public void replaceWords(int indexOfSentence,int lengthOfWord ,String newWord) throws ArrayIndexOutOfBoundsException{

        if(indexOfSentence< 0 || indexOfSentence>=textString.length-1)
            throw new ArrayIndexOutOfBoundsException();

        Pattern pattern = Pattern.compile("^[a-z]{" + lengthOfWord + "}$");
        List<String> appropriateSentence  = new ArrayList<>();
        appropriateSentence.addAll(allWords[indexOfSentence]);

        for(int index = 0; index< appropriateSentence.size(); index++){
            Matcher matcher = pattern.matcher(appropriateSentence.get(index));
            if(matcher.find()) {
                appropriateSentence.remove(index);
                appropriateSentence.add(index,newWord);
            }
        }


        System.out.print("New sentence: ");
        for(String element:appropriateSentence){
            System.out.print(element+"\t");
        }



    }
}
