package com.kurylchyk;

import java.util.HashMap;
import java.util.Map;

public class View {

    private Map<String,String> menu;

    View(){
        menu = new HashMap<>();
        menu.put("1","1 - sentence with same words");
        menu.put("2","2 - count words using split");
        menu.put("3","3 - find unique word in first sentence");
        menu.put("4","4 - sentence with question mark");
        menu.put("5","5 - find the longest word and replace with first");
        menu.put("6","6 - sort words in sentence");
        menu.put("7","7 - sort by percentage of vowels");
        menu.put("8","8 - sort only words that starts with vowel");
        menu.put("9","9 - sort by count of symbol");
        menu.put("10","10 - find the words from list");
        menu.put("11","11 - remove SubString");
        menu.put("12","12 - remove the word with appropriate length");
        menu.put("13","13 - sort words by symbol");
        menu.put("14","14 - find palindrome in text");
        menu.put("15","15 - change the words");
        menu.put("16","16 - replace words");
    }

    public void showMenu(){
        for(String element:menu.values())
                System.out.println(element);
    }
}
