package com.kurylchyk;


import java.util.Scanner;

public class Controller {
    private Text text;
    private View view;

    Controller() {
        view= new View();
        text = new Text();
        text.getText();
        text.set();

        while (true) {
            call(choose());

        }
    }


    public String choose() {
        Scanner sc = new Scanner(System.in);
        view.showMenu();
        String key = sc.nextLine();
        if (key.toUpperCase().compareTo("Q") == 0) {
            System.exit(0);
        }
        return key;
    }

    public void call(String key) {
        switch (key) {
            case "1":
                text.sentenceWithSameWords();
                break;
            case "2":
                text.countWordsUsingSplit();
                break;
            case "3": text.findUniqueWordInFirstSentence(); break;
            case "4" :text.sentenceWithQuestionMark(5); break;
            case "5": text.findTheLongestAndReplace(); break;
            case "6": text.sortWordsInSentence(); break;
            case "7":  text.sortByPercentage(); break;
            case "8": text.sortVowelWords(); break;
            case "9": text.sortByCountOfSymbol('p'); break;
            case "10": text.findTheWords(); break;
            case "11": text.removeSubString('m','o'); break;
            case "12": text.removeTheWord(5); break;
            case "13":text.sortWordsBySymbol('m'); break;
            case "14": text.findPalindrome(); break;
            case "15": text.changeTheWords(); break;
            case "16":text.replaceWords(5,5,"JAVA"); break;

        }

        clearScreen();
    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
