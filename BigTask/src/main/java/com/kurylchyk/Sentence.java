package com.kurylchyk;

import java.util.ArrayList;
import java.util.List;

public class Sentence {
  private   List<String> sentence; //set to word

    Sentence() {
        sentence = new ArrayList<>();

    }

    public void setSentence(String  str) {

        sentence.add(str);
    }

    public String getSentence(int index){
            return sentence.get(index);
    }

    public int length() {

        return sentence.size();
    }

}
